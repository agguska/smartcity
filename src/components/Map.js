import React, {Component} from 'react'
import MapView, {Marker, PROVIDER_GOOGLE} from 'react-native-maps'
import DetailScreen from '../DetailScreen'

export default class Map extends Component {

  constructor(props) {
      super(props)
      this.state = {
        latitude: this.props.startLatitude,
        longitude: this.props.startLongitude,
        markers: this.props.markers
      }
  }

  render() {
    const {longitude, latitude} = this.state
    return (
      <MapView
        provider={PROVIDER_GOOGLE}
        style={{flex: 1}}
        region={{
          latitude: latitude,
          longitude: longitude,
          latitudeDelta: 0.0922,
          longitudeDelta: 0.0421
        }}
        showsUserLocation={true}
      >
        {this.state.markers.map((marker, i) => (
          <Marker
            key={i}
            coordinate={marker.coordinate}
            title={marker.title}
            description={marker.description}
            onPress={() => this.showDetail(marker)}
          />
        ))}
    </MapView>
    );
  }

  showDetail(marker) {
    this.props.onGoToDetails(marker)

  }
}

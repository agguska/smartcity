'use strict';
import React, {Component} from 'react';
import {createBottomTabNavigator,
        createStackNavigator,
        createAppContainer } from 'react-navigation';
import {Platform, StyleSheet, Text, View, Button, TouchableOpacity} from 'react-native';
import CategoryScreen from './CategoryScreen';
import CatalogueScreen from './CatalogueScreen';
import HomeScreen from './HomeScreen';
import DetailScreen from './DetailScreen';
import Icon from 'react-native-vector-icons/Feather';

type Props = {};

const container = {
  flexDirection:'row',
  flexWrap:'wrap'
};

const styles = StyleSheet.create({
  icon: {
    paddingLeft: 10,
    paddingRight: 10
  }
});

const customNavigationOptions = {
  headerTintColor: '#000',
  headerStyle: {
    backgroundColor: '#fff',
    height: 45
  },
  headerRight: (
    <View style={container}>
      <TouchableOpacity onPress={() => navigation.navigate('Home')}>
          <Icon name='star' size={30} style={styles.icon} />
      </TouchableOpacity>
      <TouchableOpacity onPress={() => navigation.navigate('Home')}>
          <Icon name='search' size={30} style={styles.icon} />
      </TouchableOpacity>
   </View>
  ),
  headerLeft: (
    <View>
      <TouchableOpacity onPress={() => navigation.navigate('Home')}>
          <Icon name='menu' size={30} style={styles.icon} />
      </TouchableOpacity>
    </View>
  )
}

const CatalogueNavigator = createStackNavigator({
    Home: HomeScreen,
    Category: CategoryScreen,
    Catalogue: CatalogueScreen,
    Details: DetailScreen
}, {
    defaultNavigationOptions: ({ navigation }) => (customNavigationOptions)
});

const App = createAppContainer(CatalogueNavigator);
export default App;

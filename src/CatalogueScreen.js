'use strict';

import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, Butto, TouchableHighlight, ScrollView } from 'react-native';
import { fetchDataByEndpointAttributeValue } from './services/DataService';
import ServiceTile from './components/ServiceTile'
import { Badge } from 'react-native-elements'

export default class CategoryScreen extends Component {
  static navigationOptions = {
    title: 'Catalogue',
  };

  constructor(props) {
    super(props);

    this.state = {
      options: [],
      categories: []
    };
  }

  componentWillMount() {
    const {navigation} = this.props
    const chosenCategory = navigation.getParam('path', null)
    const categories = navigation.getParam('categories', [])
    this.setState({categories: categories, currentCategory: chosenCategory})
    this._loadServices(chosenCategory)
  }

  render() {
    return (
      <View>
        <View style={styles.categoryScrollContainer}>
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            {this.state.categories.map((category) => {
              return (
                <View style={{margin: 2}} key={category.id}>
                  <Badge
                    onPress={() => this._onChangeCategory(category.path)}
                    containerStyle={{backgroundColor: '#fff', borderWidth: 0.5, borderColor: 'gray'}}>
                      <Text>{category.title}</Text>
                  </Badge>
                </View>
              )
            })}
          </ScrollView>
        </View>
        <View style={styles.serviceScrollContainer}>
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            {this.state.options.map((option) => {
                return (
                  <TouchableHighlight onPress={() => this._onGoToDetails(option)} underlayColor="green" key={option.id}>
                    <View style={{margin: 10}}>
                      <ServiceTile service={option}/>
                    </View>
                  </TouchableHighlight>
                )
              })}
            </ScrollView>
         </View>
       </View>
     );
  }

  _onGoToDetails(option) {
    this.props.navigation.navigate('Details', {option: option});
  }

  _onChangeCategory(category) {
    this._loadServices(category)
  }

  _loadServices(category) {
    fetchDataByEndpointAttributeValue("options", "category", category)
      .then((res) => {
              if (res.message === 'Not Found') {
                this.setState({
                    error: 'No services found'
                });
              } else {
                this.setState({
                  error: false,
                  currentCategory: category,
                  options: res
                })
              }
        });
  }

}

const styles = StyleSheet.create({
  categoryScrollContainer: {
    marginTop: 25,
    alignItems: 'center',
  },
  serviceScrollContainer: {
    marginTop: 65,
    alignItems: 'center',
  },
  image: {
    width: 160,
    height: 170
  },
  description: {
    fontSize: 18,
    textAlign: 'center',
    color: '#656565',
    marginTop: 65
  }
});
